import {GameObject as GameObject} from './../../engine/gameObject.js';

export class Enemy
{
    /**
     * On initialisation
     * @param {CanvasRenderingContext2D} context canvas context this gameobject is on
     * @param {number|null} x_pos position x on canvas
     * @param {number|null} y_pos position y on canvas
     * @param {number|null} width width on canvas
     * @param {number|null} height height on canvas
     */
    constructor(context, x_pos=0, y_pos=0, width=32, height=32)
    {
        super(context, 2, x_pos, y_pos, width, height);
    }


    /**
     * 
     */
    update()
    {
        
    }
}