import {GameObject as GameObject} from './../engine/gameObject.js';
import { Sprite } from '../engine/sprite.js';
import{input_keys as input_keys} from './../engine/input.js';
import * as game from './../engine/game.js';
import {Bullet as Bullet} from './bullet.js';

/**
 * Represents an animated image on a canvas
 */
export class SpaceshipPlayer extends GameObject
{
    /**
     * On initialisation
     * @param {CanvasRenderingContext2D} context canvas context this gameobject is on
     * @param {number|null} x_pos position x on canvas
     * @param {number|null} y_pos position y on canvas
     * @param {number|null} width width on canvas
     * @param {number|null} height height on canvas
     * @param {number|null} speed how fast the player moves
     * @param {number|null} firing_speed how many ticks need to pass before shooting another bullet
     */
    constructor(context, x_pos=0, y_pos=0, width=32, height=32, speed=4, firing_speed=25)
    {
        super(context, 1, x_pos, y_pos, width, height);

        this.speed = speed;
        this.firing_speed = firing_speed;
        this.firing_tick = 0;
    }



    /**
     * Gets run every tick
     */
    update()
    {
        GameObject.prototype.update.call(this);
        this.movePlayer();
        this.shoot();
    }



    movePlayer()
    {
        let x_movement = 0;

        // if A pressed
        if(input_keys["65"]) x_movement -= this.speed;
        if(input_keys["68"]) x_movement += this.speed;

        this.moveTo(this.x_pos+x_movement);
        this.keepInScreen();
    }



    shoot()
    {
        // add a tick
        this.firing_tick++;

        // go to next sprite if enough ticks passed. By doing this it has a custom animation speed
        if(this.firing_tick >= this.firing_speed)
        {
            this.firing_tick = 0;  // reset tick
            let bullet = new Bullet(this.context, this.x_pos + (this.width/2), this.y_pos, 15, 15, "images/sprites/spaceship/player_bullet.png", 1.5);
        }
    }



    keepInScreen()
    {
        let max_x_pos = this.context.canvas.width - this.width;
        if(this.x_pos < 0)
            this.x_pos = 0;
        else if(this.x_pos > max_x_pos)
            this.x_pos = max_x_pos;
    }
}
