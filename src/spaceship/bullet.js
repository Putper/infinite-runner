import {GameObject as GameObject} from './../engine/gameObject.js';
import { Sprite } from '../engine/sprite.js';
import * as game from './../engine/game.js';

export class Bullet extends GameObject
{
    /**
     * On initialisation
     * @param {CanvasRenderingContext2D} context canvas context this gameobject is on
     * @param {number|null} x_pos starting position on canvas (X-as)
     * @param {number|null} y_pos starting position on canvas (Y-as)
     * @param {number|null} width width on canvas
     * @param {number|null} height height on canvas
     * @param {number|null} angle polar coordinate: where 0 is 0° (right), 0.5 is 90° (bottom), 1 is 180° (left), and 2 is 360° (right)
     * @param {number|null} speed speed of the bullet
     * @param {number|null} damage how much damage the bullet does
     */
    constructor(context, x_pos=0, y_pos=0, width=32, height=32, image="images/sprites/spaceship/player_bullet.png", angle=0.5, speed=4, damage=25)
    {
        super(context, 1, x_pos, y_pos, width, height);
        this.speed = speed;
        this.angle = angle*Math.PI;

        // create and add sprite
        let bullet_idle = new Sprite(image);
        this.addSprite(bullet_idle);

        game.game_objects.push(this); // add bullet to gameobjects
    }



    /**
     * gets called every frame
     */
    update()
    {
        GameObject.prototype.update.call(this);
        
        this.move();
        if(this.checkOutOfScreen()) this.exists = false;    // mark object as to be removed if out of screen
    }



    /**
     * move the bullet based on direction
     */
    move()
    {
        let x_direction = Math.cos(this.angle);
        let y_direction = Math.sin(this.angle);
        
        this.moveTo(
            this.x_pos + (x_direction*this.speed),
            this.y_pos + (y_direction*this.speed)
        );
    }
}