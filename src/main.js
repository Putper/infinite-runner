import * as game from './engine/game.js';
import {GameObject as GameObject} from './engine/gameObject.js';
import {Sprite as Sprite} from './engine/sprite.js';
import {SpaceshipPlayer as Player} from './spaceship/spaceshipPlayer.js';


/**
 * Creates the player
 */
function createPlayer(context)
{
    let player = new Player(context, 200, 632, 64, 64, 6);
    let player_idle = new Sprite("images/sprites/spaceship/player.png");
    player.addSprite(player_idle);
    game.game_objects.push(player);
}



// ON DOCUMENT READY
document.addEventListener('DOMContentLoaded', function()
{
    // Initialise background canvas
    let background_canvas = document.getElementById("background-canvas");
    let background_context = background_canvas.getContext("2d");
    game.contexts.push(background_context);
    // add background
    let background = new GameObject(background_context, 10, 0+(background_context.canvas.width/2), 0+(background_context.canvas.height/2), background_context.canvas.width, background_context.canvas.height);
    let background_idle = new Sprite("images/backgrounds/darknight.png");
    background.addSprite(background_idle);
    background.drawn = false;
    background.draw();
    game.game_objects.push(background);


    // Initialise main canvas
    let main_canvas = document.getElementById("main-canvas");
    let main_context = main_canvas.getContext("2d");
    game.contexts.push(main_context);

    main_context.font = "30px Arial";    // set font to be used
    createPlayer(main_context);  // create player


    
    /**
     * Main loop (repeats itself)
     */
    function loop()
    {
        window.requestAnimationFrame(loop); // Loop
        game.update();
        
        // draw timer
        main_context.fillText(
            game.timer.getTime().toFixed(1),    // round time to 1 behind decimal
            50, // x pos
            50  // y pos
        );
    }
    loop();
});