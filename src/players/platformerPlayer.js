import {GameObject as GameObject} from './../engine/gameObject.js';
import{input_keys as input_keys} from './../engine/input.js';

/**
 * Represents an animated image on a canvas
 */
export class PlatformerPlayer extends GameObject
{
    /**
     * On initialisation
     * @param {CanvasRenderingContext2D} context canvas context this gameobject is on
     * @param {number|null} category category of the sprite. 0=general, 1=players, 2=enemies, 3=collideable map
     */
    constructor(context, category=0, x_pos=0, y_pos=0, width=32, height=32, jump_height=8)
    {
        super(context, category, x_pos, y_pos, width, height);

        this.falling = false;
        this.fall_speed = 0;
        this.jumping = false;
        this.jump_height = jump_height;
        this.current_jump_height = 0;
    }


    /**
     * Gets run every tick
     * @param {array[Sprite]} game_objects array with all game_objects, used for interactions with other game objects
     */
    update(game_objects)
    {
        GameObject.prototype.update.call(this);
        this.movePlayer(game_objects);
    }


    movePlayer(game_objects)
    {
        let y_movement = 0;
        let x_movement = 0;

        y_movement += this.jumpAndFall(game_objects);

        // if A pressed
        if(input_keys["65"]) x_movement -= 4;
        if(input_keys["68"]) x_movement += 4;
        if(x_movement > 0)
            this.switchSprite("walking-right");
        else if(x_movement < 0)
            this.switchSprite("walking-left");
        else if(x_movement == 0)
            this.switchSprite("idle");

        this.moveTo(this.x_pos+x_movement, this.y_pos+y_movement);
    }

    

    jumpAndFall(game_objects)
    {
        // if space is pressed, and player is not falling; jump!
        if(input_keys["32"] && !this.falling)
        {
            this.fall_speed = 0;
            this.current_jump_height = 0;
            this.jumping = true;
        }
        
        // if player is jumping, put that dude higher in the air until he reaches his max jump height.
        if(this.jumping)
        {
            // go a little higher
            this.current_jump_height -= 1;
            this.fall_speed = -1;

            // stop with jumping if the jump height has been reached
            if(this.current_jump_height <= -this.jump_height)
            {
                this.jumping = false;
                this.falling = true;
                this.fall_speed = this.current_jump_height;  // apply gravity
            }
        }
        // if the player is falling, apply gravity.
        else if(this.falling)
        {
            this.current_jump_height = 0;
            this.fall_speed += 0.25;  // apply gravity
        }


        // check if player is on the ground. if he isnt, enable falling. if he is; disable falling.
        let collision = this.getCollisions(game_objects, 3, true, true);

        if(collision && !this.jumping)
        {
            this.falling = false;
            this.fall_speed = 0;
        }
        else
            this.falling = true;

        return this.current_jump_height + this.fall_speed;
    }
}
