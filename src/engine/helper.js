/**
 * Gets json from url and returns as object
 * @param {string} url 
 * @return {object}
 */
export function jsonRequest(url)
{
    let httpreq = new XMLHttpRequest();
    httpreq.open("GET", url, false);
    httpreq.send(null);
    return JSON.parse(httpreq.responseText);
}
