/**
 * Represents an image on a canvas
 */
export class Sprite
{
    /**
     * On initilisation of a sprite
     * @param {string} image source of the image
     * @param {string|null} name name of this sprite
     */
    constructor(image, name="idle")
    {
        // bind variables to this class
        this.name = name;

        this.image = new Image();
        this.image.src = image;

        this.hidden = false; // if the sprite is hidden or not
    }



    /**
     * Draws the sprite on the canvas
     * @param {CanvasRenderingContext2D} context canvas context to draw on
     * @param {number} x_pos x position on canvas
     * @param {number} y_pos y position on canvas
     * @param {number} width width on canvas
     * @param {number} height height on canvas
     */
    draw(context, x_pos, y_pos, width, height)
    {
        context.drawImage(
            this.image, // Image to be drawn
            x_pos, // x position on canvas
            y_pos, // Y position on canvas
            width, // width of image
            height // height of image
        );
    }
}
