/**
 * Represents a gameobject
 */
export class GameObject
{
    /**
     * On initialisation
     * @param {CanvasRenderingContext2D} context canvas context this gameobject is on
     * @param {number|null} category category of the sprite. 0=general, 1=players, 2=enemies, 3=collideable map, 10=background
     * @param {number|null} x_pos position x on canvas
     * @param {number|null} y_pos position y on canvas
     * @param {number|null} width width on canvas
     * @param {number|null} height height on canvas
     */
    constructor(context, category=0, x_pos=0, y_pos=0, width=32, height=32)
    {
        this.context = context;
        this.category = category;
        this.x_pos = x_pos - (width/2);
        this.y_pos = y_pos - (height/2);
        this.width = width;
        this.height = height;

        this.drawn = true;  // if the gameobject should be drawn
        this.exists = true; // if the gameobject should be removed or not
        this.sprites = [];  // list with all sprites
    }



    /**
     * Gets run every tick
     */
    update()
    {        
        // if there's no sprites, remove gameobject. else draw the active sprite.
        if(this.drawn && this.sprites.length > 0)
        {
            this.draw();
        }
        else    this.exists = false;
    }



    draw()
    {
        if(this.category == 10)console.log("drawing");
        this.sprites[0].draw(this.context, this.x_pos, this.y_pos, this.width, this.height);
    }


    
    addSprite(sprite)
    {
        // Only continue if no sprite with this name already exists for this gameObject
        if(this.getSprite(sprite.name) == false)
        {
            this.sprites.push(sprite);
        }
        else    return false;
    }


    getSprite(name)
    {
        for(let i=0; i<this.sprites.length; i++)
        {
            if(this.sprites[i].name == name)
                return this.sprites[i];
        }
        return false;
    }


    /**
     * Remove sprite by name
     * @param {string} name 
     */
    removeSprite(name)
    {
        let result; // where result will be stored if found

        let i = this.sprites.length;
        while(i--)
        {
            // Remove sprite if the name matches
            if(this.sprites[i].name == name)
            {
                result = this.sprites[i];
                this.sprites.splice(i, 1);
                return result;
            }
        }
        return false;   // return false if sprite wasn't found
    }



    /**
     * Sets sprite with given name as the active sprite
     * @param {string} name  name of the sprite
     */
    switchSprite(name)
    {
        if(this.sprites[0] && this.sprites[0].name !== name)   // make sure sprite is not already
        {
            let sprite = this.removeSprite(name);   // remove sprite
            if(sprite)
            {
                this.sprites.unshift(sprite); // if sprite exists move it to the first of the array
                return true;
            }
        }
        return false;   // if no sprite exists, or sprite is already active, return false
    }



    /**
     * marks this gameobject for removal
     */
    remove()
    {
        this.exists = false;
    }



    /**
     * Resizes gameobject to given size
     * @param {number|null} width 
     * @param {number|null} height 
     */
    resize(width=null, height=null)
    {
        if(width)   this.width = width;
        if(height)  this.height = height;
    }



    /**
     * Moves the gameobject to given position
     * @param {number|null} x_pos 
     * @param {number|null} y_pos 
     */
    moveTo(x_pos=null, y_pos=null)
    {
        if(x_pos !== null)   this.x_pos = x_pos;
        if(y_pos !== null)   this.y_pos = y_pos;
    }



    /**
     * Checks if the gameobject is out of screen;
     */
    checkOutOfScreen()
    {
        if(
            this.x_pos < 0 ||
            this.x_pos > this.context.width ||
            this.y_pos < 0 ||
            this.y_pos > this.context.height
        )
        {
            this.out_of_screen = true;
            return true;
        }
        else
        {
            this.out_of_screen = false;
            return false;
        }
    }



    /**
     * Gets the positions of the 4 corners of this sprite
     */
    getCornerPositions()
    {
        let corners = [];
        corners.push([this.x_pos, this.y_pos]);   // top left corner
        corners.push([this.x_pos+this.width, this.y_pos]);    // top right corner
        corners.push([this.x_pos, this.y_pos+this.height]);   // bottom left corner
        corners.push([this.x_pos+this.width, this.y_pos+this.height]);    // bottom right corner
        return corners;
    }



    /**
     * DEPRECATED Gets the game objects this game object is touching
     * @param {array[Sprite]} game_objects array with all game objects, used for interactions with other game objects
     * @param {number|null} category which category to limit to.
     * @param {boolean|null} return_first if the first result should be returned instead of returning all results
     * @param {boolean|null} feet if to only check the bottom corners
     */
    getCollisions(game_objects, category=null, return_first=false, feet=false)
    {
        let corners = this.getCornerPositions();    // positions of the corner

        let collisions = [];    // list with all game objects that this sprite collides with

        // for each srite
        for(let game_objects_index=0; game_objects_index < game_objects.length; game_objects_index++)
        {
            // for each corner of this sprite
            for(let corners_index=0; corners_index < corners.length; corners_index++)
            {
                // if we only need to check the feet, skip the top corners.
                if(feet && corners_index == 0 || corners_index == 1) continue;

                // if game object must have a specific category, but this game object doesnt have it; skip this one.
                if(category && game_objects[game_objects_index].category !== category)    continue;

                // if this corner is'nt colliding with the game object, continue. (corners[0] is the x_pos, corners[1] is the y_pos.)
                let allowed_height = (feet) ? game_objects[game_objects_index].y_pos+10 : game_objects[game_objects_index].height;
                if(
                    corners[corners_index][0] < game_objects[game_objects_index].x_pos ||
                    corners[corners_index][0] > game_objects[game_objects_index].x_pos+game_objects[game_objects_index].width ||
                    corners[corners_index][1] < game_objects[game_objects_index].y_pos ||
                    corners[corners_index][1] > allowed_height
                )   continue;

                // if the first sprite must be returned, return it. Else add it to the list.
                if(return_first)
                    return game_objects[game_objects_index];
                else
                {
                    collisions.push(game_objects[game_objects_index]);
                    break;
                }
            }
        }
        if(return_first)    return false;   // if return first is given, but none are found; return false.
        return collisions;
    }
}