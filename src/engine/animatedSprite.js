import {Sprite as Sprite} from './sprite.js';

/**
 * Represents an animated image on a canvas
 */
export class AnimatedSprite extends Sprite
{
    /**
     * On initilisation of an animated sprite.
     * @param {string} image the source of the spritesheet
     * @param {number|null} rows_amount Amount of sprites on the spritesheet on the x-as
     * @param {number|null} columns_amount Amount of sprites on the spritesheet on the y-as
     * @param {string|null} name name of this sprite
     * @param {number|null} ticks_per_frame the speed of the animation
     */
    constructor(image, name="idle", rows_amount=1, columns_amount=1, ticks_per_frame=1)
    {
        super(image, name);

        this.rows_amount = rows_amount;
        this.columns_amount = columns_amount;
        this.ticks_per_frame = ticks_per_frame;      // how many updates have to be called before going to the next frame

        this.current_row = 0;   // counter for the rows
        this.current_column = 0;    // counter for the columns
        this.current_tick = 0;  // counter for the ticks_per_frame
    }


    iterateSpritesheet()
    {
        // add a tick
        this.current_tick++;

        // go to next sprite if enough ticks passed. By doing this it has a custom animation speed
        if(this.current_tick >= this.ticks_per_frame)
        {
            this.current_tick = 0;  // reset tick
            this.current_row++; // go to next row

            // if we're past the last row of the spritesheet, go to first row and the next column
            if(this.current_row >= this.rows_amount)
            {
                this.current_row = 0;
                this.current_column++;

                // if we've passed the last column of the spritesheet, go back to the first
                if(this.current_column >= this.columns_amount)
                    this.current_column = 0;
            }
        }
    }



    /**
     * Draws the sprite on the canvas
     * @param {CanvasRenderingContext2D} context canvas context to draw on
     * @param {number} x_pos x position on canvas
     * @param {number} y_pos y position on canvas
     * @param {number} width width on canvas
     * @param {number} height height on canvas
     */
    draw(context, x_pos, y_pos, width, height)
    {
        this.iterateSpritesheet();
        context.drawImage(
            this.image, // Image to be drawn
            this.current_row*width, // x position in source image
            this.current_column*height, // Y position in source image
            width, // width in source image
            height, // height in source image
            x_pos, // x position on canvas
            y_pos, // y position on canvas
            width, // width on canvas
            height  // height on canvas
        );
    }
}
