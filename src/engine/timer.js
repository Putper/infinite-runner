export class Timer
{
    // Initialises a timer
    constructor()
    {
        this.started_on = null;
        this.time_elapsed = 0;
        this.start();
    }


    /**
     * Resets and starts the timer
     */
    start()
    {
        this.time_elapsed = 0;
        this.started_on = new Date();
    }


    /**
     * Pauses the timer
     */
    pause()
    {
        this.paused = true;
        this.started_on = null;
        this.time_elapsed += this.getTime(false);
    }


    /**
     * Returns the time that passed since timer was started
     * @param {boolean|null} total_time if the full time should be returned, or just the time since it was last paused
     */
    getTime(total_time=true)
    {
        let current_date = new Date();  // get current date/time
        let time_elapsed = (current_date - this.started_on) / 1000; // calculate time passed since last paused
        
        if(total_time)  return time_elapsed + this.time_elapsed;
        else    return time_elapsed;
    }
}