import {jsonRequest} from './helper.js';
import {Sprite as Sprite} from './sprite.js';
import {AnimatedSprite as AnimatedSprite} from './animatedSprite.js';
import {PlatformerPlayer as Player} from './../players/platformerPlayer.js';
import { GameObject } from './gameObject.js';

/**
 * Handles loading maps
 */
export class Map
{
    constructor(context)
    {
        this.context = context;
        // this.load(name);
        this.game_objects = [];
    }


    load(name)
    {
        this.game_objects = []; // empty game objects

        let player = this.createPlatformerPlayer();
        this.game_objects.push(player);  // add player to game objects

        let map = jsonRequest(name);

        // For each row
        for(let column_index=0; column_index < map.length; column_index++)
        {
            // For each column
            for(let row_index=0; row_index < map[column_index].length; row_index++)
            {
                // for each tile
                for(let tile_index=0; tile_index < map[column_index][row_index].length; tile_index++)
                {
                    let object = this.createGameObject(map[column_index][row_index][tile_index], row_index, column_index);
                    map[column_index][row_index][tile_index] = object;
                }
            }
        }
    }



    /**
     * Creates gameobject with given id
     * @param {number} id 
     */
    createGameObject(id, row=0, column=0)
    {
        let pos_x = row*32;
        let pos_y = column*32;
        let result;

        switch(id)
        {
            case "bg1":
                let background = new GameObject(this.context, 10, -1240, -150, 1920, 1080);
                let background_idle = new Sprite("images/background.png");
                background.addSprite(background_idle);
                result = background;
                break;
            case "ground-1":
                let ground_1 = new GameObject(this.context, 3, pos_x, pos_y, 32, 32);
                let ground_1_idle = new Sprite("images/sprites/platformer/floors/ground.png");
                ground_1.addSprite(ground_1_idle);
                result = ground_1;
                break;
            case "ground-3":
                let ground_3 = new GameObject(this.context, 3, pos_x, pos_y, 96, 32);
                let ground_3_idle = new Sprite("images/sprites/platformer/floors/ground.png");
                ground_3.addSprite(ground_3_idle);
                result = ground_3;
                break;
            case "ground-full":
                let ground_full = new GameObject(this.context, 3, pos_x, pos_y, 680-pos_x, 680-pos_y);
                let ground_full_idle = new Sprite("images/sprites/platformer/floors/ground.png");
                ground_full.addSprite(ground_full_idle);
                result = ground_full;
                break;
            default:
                result = null;
        }

        if(result)this.game_objects.push(result);   // if result is found, add it to the list of game objects
        return result;
    }


    
    update()
    {

    }



    /**
     * Creates the player
     */
    createPlatformerPlayer()
    {
        let player = new Player(this.context, 1, 40, 40, 32, 64);
        let player_idle = new Sprite("images/sprites/platformer/player/idle.png", "idle");
        let player_walking_left = new AnimatedSprite("images/sprites/platformer/player/walking-left.png", "walking-left", 2, 2, 7);
        let player_walking_right = new AnimatedSprite("images/sprites/platformer/player/walking-right.png", "walking-right", 2, 2, 7);
        player.addSprite(player_idle);
        player.addSprite(player_walking_left);
        player.addSprite(player_walking_right);
        return player;
    }
}