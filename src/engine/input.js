/**
 * If a key is down, it will be true. if it's false, it's not.
 * https://stackoverflow.com/a/35020537
 */
export let input_keys = {};
window.onkeyup = function(e) { input_keys[e.keyCode] = false; }
window.onkeydown = function(e) { input_keys[e.keyCode] = true; }
