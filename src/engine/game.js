import { Timer as Timer } from "./timer.js";

// Where all game objects are stored
export let game_objects = [];
export let contexts = [];
export let timer = new Timer();

/**
 * clears canvas and handles gameobjects
 */
export function update()
{
    // Clear canvases
    for(let i=0; i<contexts.length; i++)
    {
        if(contexts[i].canvas.dataset.noDraw !== "")
            contexts[i].clearRect(0, 0, contexts[i].canvas.width, contexts[i].canvas.height);
    }
    
    //  Here we loop through the array backwards so we can remove an item if needed. this way we can also put the important stuff up front
    //  https://stackoverflow.com/a/9882349
    let i = game_objects.length;
    while(i--)  // for each gameobject
    {
        game_objects[i].update(game_objects);   // run update function

        // Remove game object if it wants to be removed
        if(!game_objects[i].exists)
            game_objects.splice(i, 1);
    }
}


export function addGameObjects(gameobjects)
{
    game_objects = game_objects.concat(gameobjects);
}